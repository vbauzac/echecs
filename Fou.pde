class Fou extends Piece {
  public Fou(int couleur, char colonne, int rangee) {
    super(couleur, colonne, rangee);
    code = CODE_FOU;
  }
  
  public ArrayList<Position> getTargetPositions(){
    ArrayList<Position> positions = new ArrayList<Position>(); 
    
    // en haut à gauche
    char c = colonne;
    int r = rangee;
    while(c > 'a' && r > 1){
      c -= 1;
      r -= 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // en bas à droite
    c = colonne;
    r = rangee;
    while(c < 'h' && r < 8){
      c += 1;
      r += 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // en bas à gauche
    c = colonne;
    r = rangee;
    while(c > 'a' && r < 8){
      c -= 1;
      r += 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // en haut à droite
    c = colonne;
    r = rangee;
    while(c < 'h' && r > 1){
      c += 1;
      r -= 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    return positions;
  }
}