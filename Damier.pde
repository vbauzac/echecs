/*
 * Voir https://fr.wikipedia.org/wiki/R%C3%A8gles_du_jeu_d%27%C3%A9checs#L%27%C3%A9chiquier
 * 
 *      Noirs
 *   A B C D E F G
 * 8               8
 * 7               7
 * 6               6
 * 5               5
 * 4               4
 * 3               3
 * 2               2
 * 1               1
 *   A B C D E F G
 *      Blancs
 */

class Damier
{ 
  public void drawDamier(){
    int fh=a/3; // hauteur de fonte
     
    // Dessin du bord du plateau
    stroke(0);
    strokeWeight(1);
    fill(255);
    rect((NB_MARGIN-1)*a, (NB_MARGIN-1)*a, 10*a, 10*a, a/2);
    
    textSize(fh);
    // Boucle sur les colonnes
    for( char colonne : new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'} ){
      int colNumber = getColNumber(colonne);
      // Dessin coordonnée colonne
      fill(CASE_NOIRE);
      text(colonne, (NB_MARGIN)*a+(colNumber-1)*a+a/2-textWidth(colonne)/2, NB_MARGIN*a-a/2+fh/2);
      text(colonne, (NB_MARGIN)*a+(colNumber-1)*a+a/2-textWidth(colonne)/2, (NB_MARGIN+8)*a+a/2+fh/2);
      
      //boucle sur les rangées
      for(int rangee = 1; rangee <= 8; rangee++){
        if(colonne == 'a'){
          // Dessin coordonnée rangée (pour une seule colonne)
          fill(CASE_NOIRE);
          text(rangee, NB_MARGIN*a-a/2-textWidth(""+rangee)/2, (NB_MARGIN+8)*a-(rangee-1)*a-a/2+fh/2);
          text(rangee, (NB_MARGIN+8)*a+a/2-textWidth(""+rangee)/2, (NB_MARGIN+8)*a-(rangee-1)*a-a/2+fh/2);
        }
        
        // Dessin des cases
        noStroke();
        if( (colNumber+8*(rangee-1)+rangee)%2 == 0){
          fill(CASE_NOIRE);
        }else{
          fill(CASE_BLANCHE);
        }
        rect((NB_MARGIN+colNumber-1)*a, (8+NB_MARGIN-rangee)*a, a, a);
      }
    }
    
    // Dessin du cadre du damier
    stroke(CASE_NOIRE);
    strokeWeight(1);
    noFill();
    rect(NB_MARGIN*a, NB_MARGIN*a, 8*a, 8*a);
  }
}
