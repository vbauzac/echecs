class Roi extends Piece {
  public Roi(int couleur, char colonne, int rangee) {
    super(couleur, colonne, rangee);
    code = CODE_ROI;
  }
  
  public ArrayList<Position> getTargetPositions(){
    ArrayList<Position> positions = new ArrayList<Position>(); 
    
    if(colonne < 'h'){
      if(rangee<8){
        Piece p = partie.getPieceAt((char)(colonne+1), rangee+1);
        if(p==null || p.couleur!=couleur){
          positions.add(new Position((char)(colonne+1), rangee+1, Position.DEPLACEMENT_AVEC_PRISE));
        }
      }
      if(rangee>1){
        Piece p = partie.getPieceAt((char)(colonne+1), rangee-1);
        if(p==null || p.couleur!=couleur){
          positions.add(new Position((char)(colonne+1), rangee-1, Position.DEPLACEMENT_AVEC_PRISE));
        }
      }
      Piece p = partie.getPieceAt((char)(colonne+1), rangee);
      if(p==null || p.couleur!=couleur){
        positions.add(new Position((char)(colonne+1), rangee, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    if(colonne > 'a'){
      if(rangee<8){
        Piece p = partie.getPieceAt((char)(colonne-1), rangee+1);
        if(p==null || p.couleur!=couleur){
          positions.add(new Position((char)(colonne-1), rangee+1, Position.DEPLACEMENT_AVEC_PRISE));
        }
      }
      if(rangee>1){
        Piece p = partie.getPieceAt((char)(colonne-1), rangee-1);
        if(p==null || p.couleur!=couleur){
          positions.add(new Position((char)(colonne-1), rangee-1, Position.DEPLACEMENT_AVEC_PRISE));
        }
      }
      Piece p = partie.getPieceAt((char)(colonne-1), rangee);
      if(p==null || p.couleur!=couleur){
         positions.add(new Position((char)(colonne-1), rangee, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    if(rangee<8){
      Piece p = partie.getPieceAt(colonne, rangee+1);
      if(p==null || p.couleur!=couleur){
        positions.add(new Position(colonne, rangee+1, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    if(rangee>1){
      Piece p = partie.getPieceAt(colonne, rangee-1);
      if(p==null || p.couleur!=couleur){
        positions.add(new Position(colonne, rangee-1, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    return positions;
  }
}