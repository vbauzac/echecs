class Joueur {
  private int couleur;
  private ArrayList<Piece> pieces = new ArrayList<Piece>();
  private ArrayList<Piece> piecesSorties = new ArrayList<Piece>();
  
  public Joueur(int couleur) {
    this.couleur = couleur;

    pieces.add(new Roi(couleur, 'd', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Reine(couleur, 'e', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Tour(couleur, 'a', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Tour(couleur, 'h', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Fou(couleur, 'c', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Fou(couleur, 'f', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Cavalier(couleur, 'b', couleur == PIECE_BLANCHE ? 1 : 8) );
    pieces.add(new Cavalier(couleur, 'g', couleur == PIECE_BLANCHE ? 1 : 8) );
    for( char col : new char[]{'a','b','c','d','e','f','g','h'}) {
      pieces.add(new Pion(couleur, col, couleur == PIECE_BLANCHE ? 2 : 7) );
    }
  }
  
  public void drawJoueur(){
    for( Piece piece : pieces ){
      piece.drawPiece();
    }
    
    int idx = 0;
    for( Piece piece : piecesSorties ){
      piece.drawPieceSortie(idx++);
    }
  }
  
   public void resetHighlight(){
    for( Piece piece : pieces ){
      piece.setHighlighted(false);
    }
  }
  
  public ArrayList<Piece> getPieces(){
    return pieces;
  }
  
  public Roi getKing(){
    for(Piece p : pieces){
      if(p.code == Piece.CODE_ROI){
        return (Roi)p;
      }
    }
    return null;
  }
  
  public void sortirPiece(Piece pieceASortir){
    pieces.remove(pieceASortir);
    piecesSorties.add(pieceASortir);
    pieceASortir.setSelected(false);
    pieceASortir.setHighlighted(false);
    pieceASortir.deplace('z',0);
  }
  
  public ArrayList<Piece> getPiecesSorties(){
    return piecesSorties;
  }
}