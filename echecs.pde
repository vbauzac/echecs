 
  // Déclaration des constantes (final=non modifiable)
  final int CASE_BLANCHE = #FADD91;
  final int CASE_NOIRE = #A76E37;
  final int PIECE_BLANCHE = #FFFFFF;
  final int PIECE_NOIRE = #000000;
  final int FRAME_RATE  = 24; // default = 60
  final int NB_MARGIN = 3;
  final int BACKGROUND = 200;

  // déclaration des variables globale
  Partie partie;
  int a; // taille carreau
    
  /**
  * Initialisation de l'environnement graphique
  * Instanciation de la variables globale Partie.
  *   Pour commencer une nouvelle partie, il faut donc relancer le programme.
  *   Cette instanciation pourrait être gérée via un menu, permettant ainsi le démarrage
  *   d'une nouvelle partie sans avoir à relancer le programme.
  *   La variable a (taille d'1 carreau et des bordures) est recalculée à chaque draw() 
  *   pour s'adapter à un changement de la taille de la fenêtre
  *   La taille initiale de la fenêtre est fixée à 600x600
  *   On autorise le redimensionnement de la fenêtre (non autorisé par défaut)
  *   Le taux de rafraichissement (fréquence d'appel à la méthode draw()) est fixé à 24 fois 
  *   par seconde ... largement suffisant pour un comportement fluide et plus léger pour le 
  *   processeur que la valeur par défaut (60). Etant donné la vitesse de réaction d'un joueur 
  *   d'échec moyen, il est tout à fait possible de baisser cette valeur si nécessaire ... 
  **/
  void setup() {
    size(600, 600);
    frame.setResizable(true);
    frameRate(FRAME_RATE);
    partie = new Partie();
  }
 
 /**
 * Dessin de la partie, après recalcul de la taille d'1 carrreau (voir ci-dessus)
 **/
 void draw () {
   a = min(width, height) / (8+2*NB_MARGIN);
   background(BACKGROUND);
   partie.drawPartie();
 }
 
 /**
 * Détection d'un click souris
 * L'évennement est simplement transmis à la partie qui se chargera de sa gestion
 **/
 void mouseClicked() {
   partie.manageClick();
 }
 
 /**
 * Fonction globale qui permet de déterminer le numéro d'ordre d'une colonne à partir de son nom (de 'a' à 'h')
 * Un char est en fait un octet dont la valeur est le code ASCII du caractère
 * Les codes ASCII des noms de colonnes se suivent, dans l'ordre de manière continue ...
 * Il suffit donc de retrancher ('a'-1) au code ASCII du nom d'une colonne pour avoir son numéro d'ordre
 * Cette méthode étant utilisée dans différentes classes, il est préférable quelle soit globale afin d'éviter de répéter le code.
 **/
 int getColNumber(char colonne){
   return (int) colonne-('a'-1);
 }
 