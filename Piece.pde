abstract class Piece {
  static public final char CODE_ROI = '\u265A';
  static public final char CODE_REINE = '\u265B';
  static public final char CODE_TOUR = '\u265C';
  static public final char CODE_FOU = '\u265D';
  static public final char CODE_CAVALIER = '\u265E';
  static public final char CODE_PION = '\u265F';
  static public final int SELECTION_COLOR = #FF0000;
  static public final int HIGHLIGHT_COLOR = #00FF00;
  
  char colonne;
  int rangee;
  int couleur;
  final PFont font = createFont("Arial Unicode MS", 32);
  char code;
  boolean selected = false;
  boolean highlighted = false;
  
  public Piece(int couleur, char colonne, int rangee) {
    this.couleur = couleur;
    this.colonne = colonne;
    this.rangee = rangee;
  }
  
  public abstract ArrayList<Position> getTargetPositions();
   
  public void drawPiece(){
    int fh = (int) (2*a/3); // hauteur de fonte
    int colnum = getColNumber(colonne);
    textFont(font,fh);
   
    // Test si la souris est sur la case de cette piece ou si la piece est sélectionnée pour mettre la pièce en surbrillance (priorité à la couleur de sélection)
    if(selected){
      fill(SELECTION_COLOR);
    }else if(highlighted){
      fill(HIGHLIGHT_COLOR);
    }else{
      fill(couleur);
    }
    text(code, NB_MARGIN*a+(colnum-1)*a+a/2-textWidth(code)/2, (NB_MARGIN+8)*a-rangee*a+a/2+fh/4);
  }
  
  public void drawPieceSortie(int idx){
    int fh = (int) (2*a/3); // hauteur de fonte
    textFont(font,fh);
    fill(couleur);
    int x, y;
    if(idx<8){
      x=(int)(NB_MARGIN*a+idx*a+a/2-textWidth(code)/2);
    }else{
      x=(int)(NB_MARGIN*a+(idx-8)*a+a/2-textWidth(code)/2);
    }
    
    if(couleur == PIECE_BLANCHE){
      if(idx<8){
        y=NB_MARGIN*a-3*a/2+fh/2;
      }else{
        y=NB_MARGIN*a-5*a/2+fh/2;
      }
    }else{
      if(idx<8){
        y=NB_MARGIN*a+9*a+a/2+fh/2;//height-2*a+fh/2;
      }else{
        y=NB_MARGIN*a+9*a+3*a/2+fh/2;//height-a+fh/2;
      }
    }
    text(code, x, y);
  }
  
  public boolean isAtPosition(char colonne, int rangee){
    return this.colonne == colonne && this.rangee == rangee;
  }
  
  public boolean isAtLocation(int x, int y){
    int colnum = getColNumber(colonne);
    return x>=NB_MARGIN*a+(colnum-1)*a && x<=NB_MARGIN*a+colnum*a && y>=(NB_MARGIN+8)*a-rangee*a && y<=(NB_MARGIN+8)*a-(rangee-1)*a;
  }
  
  public boolean isSelected(){
    return this.selected;
  }
  
  public void setSelected(boolean selected){
    this.selected = selected;
  }
  
  public boolean isHighlighted(){
    return this.highlighted;
  }
  
  public void setHighlighted(boolean highlighted){
    this.highlighted = highlighted;
  }
   
  public void deplace(char colonne, int rangee){
    this.colonne = colonne;
    this.rangee = rangee;
    this.selected=false;
  }   
}