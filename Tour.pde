class Tour extends Piece {
  public Tour(int couleur, char colonne, int rangee) {
    super(couleur, colonne, rangee);
    code = CODE_TOUR;
  }
  
  public ArrayList<Position> getTargetPositions(){
    ArrayList<Position> positions = new ArrayList<Position>(); 
    
    // à gauche
    char c = colonne;
    int r = rangee;
    while(c > 'a'){
      c -= 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // à droite
    c = colonne;
    r = rangee;
    while(c < 'h'){
      c += 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // en bas
    c = colonne;
    r = rangee;
    while(r < 8){
      r += 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    // en haut
    c = colonne;
    r = rangee;
    while(r > 1){
      r -= 1;
      Piece p = partie.getPieceAt(c, r);
      if( p != null){
        if(p.couleur != this.couleur){
          positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
        }
        break;
      }else{
        positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    return positions;
  }
}