class Pion extends Piece {
  private boolean premierDeplacement = true;
  
  public Pion(int couleur, char colonne, int rangee) {
    super(couleur, colonne, rangee);
    code = CODE_PION;
  }
 
  public ArrayList<Position> getTargetPositions(){
    ArrayList<Position> positions = new ArrayList<Position>();
    
    // les déplacements seul: 1 case en avant ou 2 si c'est le premier déplacement
    if(couleur == PIECE_BLANCHE){
      char c = colonne;
      int r = rangee+1;
      Piece p = partie.getPieceAt(c, r);
      if(r <= 8 && p == null){
        positions.add(new Position(c, r, Position.DEPLACEMENT_SEUL));
      }
      if(premierDeplacement && p==null){
        c = colonne;
        r = rangee+2;
        p = partie.getPieceAt(c, r);
        if(r <= 8 && p == null){
          positions.add(new Position(c, r, Position.DEPLACEMENT_SEUL));
        }
      }
    }else if(couleur == PIECE_NOIRE){
      char c = colonne;
      int r = rangee-1;
      Piece p = partie.getPieceAt(c, r);
      if(r >= 1 && p == null){
        positions.add(new Position(c, r, Position.DEPLACEMENT_SEUL));
      }
      if(premierDeplacement && p==null){
        c = colonne;
        r = rangee-2;
        p = partie.getPieceAt(c, r);
        if(r >= 1 && p == null){
          positions.add(new Position(c, r, Position.DEPLACEMENT_SEUL));
        }
      }
    }
    
    // les déplacement avec prise obligée, en oblique vers l'avant
    if(couleur == PIECE_BLANCHE){
      if(rangee < 8){
        if(colonne > 'a'){
          char c = (char) (colonne-1);
          int r = rangee+1;
          Piece p = partie.getPieceAt(c, r);
          if( p != null && p.couleur != this.couleur){
            positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE_OBLIGEE));
          }
        }
        if(colonne < 'h'){
          char c = (char) (colonne+1);
          int r = rangee+1;
          Piece p = partie.getPieceAt(c, r);
          if( p != null && p.couleur != this.couleur){
            positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE_OBLIGEE));
          }
        }
      }
    }else if(couleur == PIECE_NOIRE){
      if(rangee > 1){
        if(colonne > 'a'){
          char c = (char) (colonne-1);
          int r = rangee-1;
          Piece p = partie.getPieceAt(c, r);
          if( p != null && p.couleur != this.couleur){
            positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE_OBLIGEE));
          }
        }
        if(colonne < 'h'){
          char c = (char) (colonne+1);
          int r = rangee-1;
          Piece p = partie.getPieceAt(c, r);
          if( p != null && p.couleur != this.couleur){
            positions.add(new Position(c, r, Position.DEPLACEMENT_AVEC_PRISE_OBLIGEE));
          }
        }
      }
    }
    return positions;
  }
  
  public void deplace(char colonne, int rangee){
    super.deplace(colonne, rangee);
    premierDeplacement = false;
  }
}