class Cavalier extends Piece {
  public Cavalier(int couleur, char colonne, int rangee) {
    super(couleur, colonne, rangee);
    code = CODE_CAVALIER;
  }
  
  public ArrayList<Position> getTargetPositions(){
    ArrayList<Position> positions = new ArrayList<Position>(); 
    
     if(colonne>'a'){
      if(rangee>2){
        positions.add(new Position((char)(colonne-1), rangee-2, Position.DEPLACEMENT_AVEC_PRISE));
      }
      if(rangee<7){
        positions.add(new Position((char)(colonne-1), rangee+2, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    if(colonne>'b'){
      if(rangee>1){
        positions.add(new Position((char)(colonne-2), rangee-1, Position.DEPLACEMENT_AVEC_PRISE));
      }
      if(rangee<8){
        positions.add(new Position((char)(colonne-2), rangee+1, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    if(colonne<'h'){
      if(rangee>2){
        positions.add(new Position((char)(colonne+1), rangee-2, Position.DEPLACEMENT_AVEC_PRISE));
      }
      if(rangee<7){
        positions.add(new Position((char)(colonne+1), rangee+2, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
 
    if(colonne<'g'){
      if(rangee>1){
        positions.add(new Position((char)(colonne+2), rangee-1, Position.DEPLACEMENT_AVEC_PRISE));
      }
      if(rangee<8){
        positions.add(new Position((char)(colonne+2), rangee+1, Position.DEPLACEMENT_AVEC_PRISE));
      }
    }
    
    return positions;
  }
}
