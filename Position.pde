class Position{
 static public final int DEPLACEMENT_SEUL = 0;
 static public final int DEPLACEMENT_AVEC_PRISE = 1;
 static public final int DEPLACEMENT_AVEC_PRISE_OBLIGEE = 2;
  
 private char colonne;
 private int rangee;
 private int typeDeplacement = -1;
 
 public Position(char colonne, int rangee){
   this.colonne = colonne;
   this.rangee = rangee;
 }
 
 public Position(char colonne, int rangee, int typeDeplacement){
   this(colonne, rangee);
   this.typeDeplacement = typeDeplacement;
 }
 
 public char getColonne(){
   return colonne;
 }
 
 public int getRangee(){
   return rangee;
 }
 
 public int getLeftX(){
   return NB_MARGIN*a+(getColNumber(colonne)-1)*a;
 }
 
 public int getUpperY(){
   return (NB_MARGIN+8)*a-rangee*a;
 }  
 
 public int getTypeDeplacement(){
   return typeDeplacement;
 }
 
  public boolean isAtLocation(int x, int y){
    int colnum = getColNumber(colonne);
    return x>=NB_MARGIN*a+(colnum-1)*a && x<=NB_MARGIN*a+colnum*a && y>=(NB_MARGIN+8)*a-rangee*a && y<=(NB_MARGIN+8)*a-(rangee-1)*a;
  }
 
 public void drawPosition(int mycolor){
   stroke(mycolor);
   strokeWeight(4);
   noFill();
   
   if(typeDeplacement == DEPLACEMENT_SEUL){
     line(getLeftX()+a/4, getUpperY()+a/4, getLeftX()+3*a/4, getUpperY()+3*a/4);
     line(getLeftX()+a/4, getUpperY()+3*a/4, getLeftX()+3*a/4, getUpperY()+a/4);
   }else if(typeDeplacement == DEPLACEMENT_AVEC_PRISE || typeDeplacement == DEPLACEMENT_AVEC_PRISE_OBLIGEE){
     ellipse(getLeftX()+a/2, getUpperY()+a/2, a/2, a/2);
   }
 }
}
