class Partie {
  Joueur blancs, noirs;
  Damier damier;
  Joueur playingJoueur;
  boolean echec = false;
  boolean mat = false;
  
  public Partie(){
    // Création des joueurs
    blancs = new Joueur(PIECE_BLANCHE);
    noirs = new Joueur(PIECE_NOIRE);
    // Création du damier
    damier = new Damier();
    // C'est toujours les blancs qui commencent
    playingJoueur = blancs;
  }
  
  public void drawPartie(){
    // On enlève la surbrillance sur toutes les pièces
    blancs.resetHighlight();
    noirs.resetHighlight();
        
    // l'affichage va être modifié en fonction de la position de la souris
    // On commence donc par déterminer ce qui est sous la souris.
    // Quelle pièce sous la souris?
    Piece pieceSurvolee = getPieceAtLocation(mouseX, mouseY);
    // Quelle target position sous la souris
    Position positionSurvolee = getTargetPositionAtlocation(mouseX, mouseY);
    
    if( pieceSurvolee != null ){
      // Il y a une pièce sous la souris
      // Mise en surbrillance
      pieceSurvolee.setHighlighted(true);
      // gestion du curseur
      if(pieceSurvolee.couleur == playingJoueur.couleur){
        cursor(HAND);
      }else{
        if( positionSurvolee != null ){
          cursor(HAND);
        }else{
          cursor(ARROW);
        }
      }
    }else{
      // il n'y a pas de pièce sous la souris
      // mais il y a une destination possible pour la pièce sélectionnée
      if( positionSurvolee != null ){
        cursor(HAND);
      }else{
        cursor(ARROW);
      }
    }
    
    // Dessin du jeu
    damier.drawDamier();
    blancs.drawJoueur();
    noirs.drawJoueur();
    
    // Dessin des destinations possibles pour la piece survolée
    if( pieceSurvolee != null ){     
      // Dessin des destinations possibles de la piece qui est sous la souris
      for( Position position : pieceSurvolee.getTargetPositions()){
        Piece pieceCible = getPieceAt(position);
        if(pieceCible==null || pieceCible.couleur != pieceSurvolee.couleur){
          position.drawPosition(Piece.HIGHLIGHT_COLOR);
        }
      }
    }
    
    // Dessin des destinations possibles de la piece sélectionnée
    Piece selectedPiece = getSelectedPiece();
    if(selectedPiece != null){
      for(Position position : selectedPiece.getTargetPositions()){
        Piece pieceCible = getPieceAt(position);
        if(pieceCible==null || pieceCible.couleur != selectedPiece.couleur){
          position.drawPosition(Piece.SELECTION_COLOR);
        }
      }
    }
    
    // indicateur du joueur qui joue
    fill(0);
    textSize(a/3);
    if(playingJoueur == blancs){
      text((echec?"Echec! ":"")+"C'est aux blancs de jouer", a/2, a/2);
    }else{
      text((echec?"Echec! ":"")+"C'est aux noirs de jouer", a/2, a/2);
    }    
  }
  
  private Piece getPieceAt(Position position){
    return getPieceAt(position.getColonne(), position.getRangee());
  }
  
  private Piece getPieceAt(char colonne, int rangee){
    for(Piece p : blancs.getPieces()){
      if(p.isAtPosition(colonne,rangee)){
        return p;
      }
    }
    for(Piece p : noirs.getPieces()){
      if(p.isAtPosition(colonne,rangee)){
        return p;
      }
    }
    return null;
  }
  
  private Piece getPieceAtLocation(int x, int y){
    for(Piece p : blancs.getPieces()){
      if(p.isAtLocation(x,y)){
        return p;
      }
    }
    for(Piece p : noirs.getPieces()){
      if(p.isAtLocation(x,y)){
        return p;
      }
    }
    return null;
  }
  
  private Position getTargetPositionAtlocation(int x, int y){
    Piece selectedPiece = getSelectedPiece();
    if( selectedPiece != null){
      ArrayList<Position> selectedPieceTargets = selectedPiece.getTargetPositions();
      if(selectedPieceTargets != null){
        for( Position p : selectedPieceTargets){
          if(p.isAtLocation(x,y)){
            return p;
          }
        }
      }
    }
    return null;
  }

  private Piece getSelectedPiece(){
    for(Piece p : blancs.getPieces()){
      if(p.isSelected()){
        return p;
      }
    }
    for(Piece p : noirs.getPieces()){
      if(p.isSelected()){
        return p;
      }
    }   
    return null;
  }
  
  /**
  * Gestion d'un click souris, détecté au niveau global et simplement transmis ici
  */ 
  void manageClick() {
    Piece clickedPiece = getPieceAtLocation(mouseX, mouseY);
    Position clickedTargetPosition = getTargetPositionAtlocation(mouseX, mouseY);
    Piece selectedPiece = getSelectedPiece();
    boolean coupJoue = false;
    
    // fin de partie ...
    if(mat){
      return;
    }

    if(clickedPiece == null && clickedTargetPosition == null && selectedPiece == null){
      // on a cliqué dans le vide
    }else if(clickedPiece == null && clickedTargetPosition == null && selectedPiece != null){
      // on a cliqué dans le vide
    }else if(clickedPiece == null && clickedTargetPosition != null && selectedPiece == null){
      // normalement pas possible: si on a une targetposition c'est qu'on a une pièce sélectionnée
      println("Erreur: click sur une target position alors qu'on n'a pas de pièce sélectionnée!");
    }else if(clickedPiece == null && clickedTargetPosition != null && selectedPiece != null){
      // on a cliqué sur une target posistion de la pièce sélectionnée et il n'y a pas de pièce adverse dessous
      // on déplace la pièce sélectionnée (ce qui la déselectionne)
      selectedPiece.deplace(clickedTargetPosition.colonne, clickedTargetPosition.rangee);
      coupJoue = true;
    }else if(clickedPiece != null && clickedTargetPosition == null && selectedPiece == null){
      // on a cliqué sur une pièce et il n'y a pas de pièce sélectionnée
      if(clickedPiece.couleur == playingJoueur.couleur){
        // c'est une pièce du joueur qui joue => on la sélectionne
        clickedPiece.setSelected(true);
      }else{
        // c'est une pièce du joueur qui ne joue pas mais pas sur une taget posistion => on fait rien
      }
    }else if(clickedPiece != null && clickedTargetPosition == null && selectedPiece != null){
      // on a cliqué sur une pièce et il y a une pièce sélectionnée maios ce n'est pas une target position
      if(clickedPiece.couleur == playingJoueur.couleur){
        // c'est une pièce du joueur qui joue
        if(clickedPiece == selectedPiece){
          // c'est la pièce sélectionnée => on la déselectionne
          selectedPiece.setSelected(false);
        }else{
          // c'est une autre pièce => on la selectionne et on déselectionne la pièce précédemment sélectionnée
          selectedPiece.setSelected(false);
          clickedPiece.setSelected(true);
        }
      }else{
        // c'est une pièce du joueur qui ne joue pas mais pas sur une taget posistion => on fait rien
      }      
    }else if(clickedPiece != null && clickedTargetPosition != null && selectedPiece == null){
      // normalement pas possible: si on a une targetposition c'est qu'on a une pièce sélectionnée
      println("Erreur: click sur une target position alors qu'on n'a pas de pièce sélectionnée!");
    }else if(clickedPiece != null && clickedTargetPosition != null && selectedPiece != null){
      // on a cliqué sur une pièce qui est une target posistion => on enlève la pièce sur la target et on déplace la pièce sélectionné
      (playingJoueur==blancs?noirs:blancs).sortirPiece(clickedPiece);
      selectedPiece.deplace(clickedTargetPosition.colonne, clickedTargetPosition.rangee);
      coupJoue = true;
    } 
    
    // Si un coup a été joué => on vérifie echec et mat et on change de joueur
    if(coupJoue){

      Joueur adversaire = (playingJoueur==blancs?noirs:blancs);
      Roi roiAdverse = adversaire.getKing();
      
      // echec?
      if(roiAdverse != null){
        echec=false;
        flag:
        for(Piece piece : playingJoueur.getPieces()){
          for(Position position : piece.getTargetPositions()){
            if( (position.getTypeDeplacement() == Position.DEPLACEMENT_AVEC_PRISE || position.getTypeDeplacement() == Position.DEPLACEMENT_AVEC_PRISE_OBLIGEE) 
              && position.colonne == roiAdverse.colonne
              && position.rangee == roiAdverse.rangee){
                echec = true;
                break flag;
            }
          }
        }
      }
      
      // changement de joueur
      playingJoueur = (playingJoueur==blancs?noirs:blancs);
    }
  }
}